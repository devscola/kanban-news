package kanban

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

var apiKey = os.Getenv("API_KEY")
var apiToken = os.Getenv("API_TOKEN")
var apiBaseURL = "https://api.trello.com/1"
var kanbanID = os.Getenv("KANBAN_ID")
var callbackURL = os.Getenv("CALLBACK_URL")

// Connect connects to Kanban board
func Connect() {
	board := findBoard()
	webhook := createWebhook(board)

	message := fmt.Sprintf("Connected to board %s via webhook %s", board, webhook)
	fmt.Println(message)
}

func findBoard() string {
	boardURL := fmt.Sprintf("%s/boards/%s?key=%s&token=%s", apiBaseURL, kanbanID, apiKey, apiToken)
	resp, err := http.Get(boardURL)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic("Board not found")
	}
	board := data.(map[string]interface{})

	return board["id"].(string)
}

func createWebhook(board string) map[string]interface{} {
	webhookData := map[string]string{
		"description": "kanban-news",
		"callbackURL": callbackURL,
		"idModel":     board,
	}
	jsonData, err := json.Marshal(webhookData)
	webhookURL := fmt.Sprintf("%s/webhooks/?key=%s&token=%s", apiBaseURL, apiKey, apiToken)
	resp, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		fmt.Println(resp.Status)
		fmt.Println(resp.Body)

		panic("Webhook cannot be created")
	}

	body, err := ioutil.ReadAll(resp.Body)

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic("Webhook data cannot be parsed")
	}

	return data.(map[string]interface{})
}
