package interpreter

import (
	"encoding/json"
	"fmt"
)

// Interpret interprets klingon and returns an human message
func Interpret(input []byte) (string, error) {
	data := parse(input)

	action := fetch(data["action"])
	messageType := action["type"]

	if messageType == "createCard" {
		data := fetch(action["data"])
		card := fetch(data["card"])
		person := fetch(action["memberCreator"])
		list := fetch(data["list"])
		cardLink := createShortlink(card["shortLink"].(string))

		return fmt.Sprintf("**%s (%s)** created card **%s** on list **%s**:\n%s", person["fullName"], person["username"], card["name"], list["name"], cardLink), nil
	} else if messageType == "commentCard" {
		data := fetch(action["data"])
		card := fetch(data["card"])
		person := fetch(action["memberCreator"])
		comment := data["text"]
		cardLink := createShortlink(card["shortLink"].(string))

		return fmt.Sprintf("**%s (%s)** commented on **%s**:\n>%s\n%s", person["fullName"], person["username"], card["name"], comment, cardLink), nil
	} else if messageType == "updateCard" {
		data := fetch(action["data"])
		if contains(data["listBefore"]) && contains(data["listAfter"]) {
			card := fetch(data["card"])
			person := fetch(action["memberCreator"])
			oldList := fetch(data["listBefore"])
			newList := fetch(data["listAfter"])
			cardLink := createShortlink(card["shortLink"].(string))

			return fmt.Sprintf("**%s (%s)** moved card **%s** from list **%s** to list **%s**:\n%s", person["fullName"], person["username"], card["name"], oldList["name"], newList["name"], cardLink), nil
		}
	}

	fmt.Println(string(input))
	return "", fmt.Errorf("**%s** action not supported", action["type"])
}

func parse(input []byte) map[string]interface{} {
	var data map[string]interface{}
	json.Unmarshal(input, &data)
	return data
}

func createShortlink(id string) string {
	return "https://trello.com/c/" + id
}

func fetch(data interface{}) map[string]interface{} {
	return data.(map[string]interface{})
}

func contains(data interface{}) bool {
	return data != nil
}
