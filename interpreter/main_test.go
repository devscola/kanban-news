package interpreter_test

import (
	"testing"

	"gitlab.com/devscola/kanban-news/interpreter"
)

func TestInterpreterUnderstandsAddingAComment(t *testing.T) {
	input := "{\"action\":{\"data\":{\"board\":{\"id\":\"55152efeca261dd348790ec6\",\"name\":\"Devscola Kanban\",\"shortLink\":\"umDj63lY\"},\"card\":{\"id\":\"5d6e9809310ed836c45d81c2\",\"idShort\":471,\"name\":\"Mejorar integración trello \u003c\u003e mattermost\",\"shortLink\":\"efhMxG9i\"},\"list\":{\"id\":\"589a0ea1c6ae25901de6d73a\",\"name\":\"TO Interpret\"},\"text\":\"Estoy en ello, intento tenerlo para la siguiente asamblea.\"},\"date\":\"2019-09-27T11:29:11.113Z\",\"display\":{\"entities\":{\"card\":{\"hideIfContext\":true,\"id\":\"5d6e9809310ed836c45d81c2\",\"shortLink\":\"efhMxG9i\",\"text\":\"Mejorar integración trello \u003c\u003emattermost\",\"type\":\"card\"},\"comment\":{\"text\":\"Estoy en ello, intento tenerlo para la siguiente asamblea.\",\"type\":\"comment\"},\"contextOn\":{\"hideIfContext\":true,\"idContext\":\"5d6e9809310ed836c45d81c2\",\"translationKey\":\"action_on\",\"type\":\"translatable\"},\"memberCreator\":{\"id\":\"5204f6d9688f3e662400192a\",\"text\":\"Vicente Baixauli\",\"type\":\"member\",\"username\":\"vibaiher\"}},\"translationKey\":\"action_comment_on_card\"},\"id\":\"5d8df287d31a1317fd316d05\",\"idMemberCreator\":\"5204f6d9688f3e662400192a\",\"limits\":{\"reactions\":{\"perAction\":{\"disableAt\":1000,\"status\":\"ok\",\"warnAt\":900},\"uniquePerAction\":{\"disableAt\":17,\"status\":\"ok\",\"warnAt\":16}}},\"memberCreator\":{\"activityBlocked\":false,\"avatarHash\":\"d3384d68d656f87d013cedc39b3a5382\",\"avatarUrl\":\"https://trello-avatars.s3.amazonaws.com/d3384d68d656f87d013cedc39b3a5382\",\"fullName\":\"Vicente Baixauli\",\"id\":\"5204f6d9688f3e662400192a\",\"idMemberReferrer\":null,\"initials\":\"VB\",\"nonPublic\":{},\"nonPublicAvailable\":false,\"username\":\"vibaiher\"},\"type\":\"commentCard\"},\"model\":{}}"
	expected := "**Vicente Baixauli (vibaiher)** commented on **Mejorar integración trello <> mattermost**:\n>Estoy en ello, intento tenerlo para la siguiente asamblea.\nhttps://trello.com/c/efhMxG9i"

	actual, _ := interpreter.Interpret([]byte(input))

	if actual != expected {
		t.Errorf("Expected '%s', got '%s'", expected, actual)
	}
}

func TestInterpreterUnderstandsCardCreation(t *testing.T) {
	input := "{\"action\":{\"id\":\"5db1c2de51733357f1233c55\",\"idMemberCreator\":\"5204f6d9688f3e662400192a\",\"data\":{\"card\":{\"id\":\"5db1c2dd51733357f1233c51\",\"name\":\"Asamblea: 01 Octubre 2019\",\"idShort\":485,\"shortLink\":\"8pokLGPD\"},\"list\":{\"id\":\"57f36b227295309e6cea12a3\",\"name\":\"ASAMBLEAS\"},\"board\":{\"id\":\"55152efeca261dd348790ec6\",\"name\":\"Devscola Kanban\",\"shortLink\":\"umDj63lY\"}},\"type\":\"createCard\",\"date\":\"2019-10-24T15:27:26.030Z\",\"limits\":{},\"display\":{\"translationKey\":\"action_create_card\",\"entities\":{\"card\":{\"type\":\"card\",\"id\":\"5db1c2dd51733357f1233c51\",\"shortLink\":\"8pokLGPD\",\"text\":\"Nueva tarjeta\"},\"list\":{\"type\":\"list\",\"id\":\"57f36b227295309e6cea12a3\",\"text\":\"IN PROGRESS\"},\"memberCreator\":{\"type\":\"member\",\"id\":\"5204f6d9688f3e662400192a\",\"username\":\"vibaiher\",\"text\":\"Vicente Baixauli\"}}},\"memberCreator\":{\"id\":\"5204f6d9688f3e662400192a\",\"activityBlocked\":false,\"avatarHash\":\"d3384d68d656f87d013cedc39b3a5382\",\"avatarUrl\":\"https://trello-avatars.s3.amazonaws.com/d3384d68d656f87d013cedc39b3a5382\",\"fullName\":\"Vicente Baixauli\",\"idMemberReferrer\":null,\"initials\":\"VB\",\"nonPublic\":{},\"nonPublicAvailable\":false,\"username\":\"vibaiher\"}}}"
	expected := "**Vicente Baixauli (vibaiher)** created card **Asamblea: 01 Octubre 2019** on list **ASAMBLEAS**:\nhttps://trello.com/c/8pokLGPD"

	actual, _ := interpreter.Interpret([]byte(input))

	if actual != expected {
		t.Errorf("Expected '%s', got '%s'", expected, actual)
	}
}

func TestInterpreterUnderstandsCardMovements(t *testing.T) {
	input := "{\"action\":{\"id\":\"5db1cbf29112204b2934a1bb\",\"idMemberCreator\":\"5204f6d9688f3e662400192a\",\"data\":{\"old\":{\"idList\":\"58179a0def5d4e043a29fbf4\"},\"card\":{\"idList\":\"57f36b227295309e6cea12a3\",\"id\":\"5d6e9809310ed836c45d81c2\",\"name\":\"Mejorar integración trello <>mattermost\",\"idShort\":471,\"shortLink\":\"efhMxG9i\"},\"board\":{\"id\":\"55152efeca261dd348790ec6\",\"name\":\"Devscola Kanban\",\"shortLink\":\"umDj63lY\"},\"listBefore\":{\"id\":\"58179a0def5d4e043a29fbf4\",\"name\":\"TO DO\"},\"listAfter\":{\"id\":\"57f36b227295309e6cea12a3\",\"name\":\"IN PROGRESS\"}},\"type\":\"updateCard\",\"date\":\"2019-10-24T16:06:10.722Z\",\"limits\":{},\"display\":{\"translationKey\":\"action_move_card_from_list_to_list\",\"entities\":{\"card\":{\"type\":\"card\",\"idList\":\"57f36b227295309e6cea12a3\",\"id\":\"5d6e9809310ed836c45d81c2\",\"shortLink\":\"efhMxG9i\",\"text\":\"Mejorar integración trello <>mattermost\"},\"listBefore\":{\"type\":\"list\",\"id\":\"58179a0def5d4e043a29fbf4\",\"text\":\"DONE\"},\"listAfter\":{\"type\":\"list\",\"id\":\"57f36b227295309e6cea12a3\",\"text\":\"IN PROGRESS\"},\"memberCreator\":{\"type\":\"member\",\"id\":\"5204f6d9688f3e662400192a\",\"username\":\"vibaiher\",\"text\":\"Vicente Baixauli\"}}},\"memberCreator\":{\"id\":\"5204f6d9688f3e662400192a\",\"activityBlocked\":false,\"avatarHash\":\"d3384d68d656f87d013cedc39b3a5382\",\"avatarUrl\":\"https://trello-avatars.s3.amazonaws.com/d3384d68d656f87d013cedc39b3a5382\",\"fullName\":\"Vicente Baixauli\",\"idMemberReferrer\":null,\"initials\":\"VB\",\"nonPublic\":{},\"nonPublicAvailable\":false,\"username\":\"vibaiher\"}}}"
	expected := "**Vicente Baixauli (vibaiher)** moved card **Mejorar integración trello <>mattermost** from list **TO DO** to list **IN PROGRESS**:\nhttps://trello.com/c/efhMxG9i"

	actual, _ := interpreter.Interpret([]byte(input))

	if actual != expected {
		t.Errorf("Expected '%s', got '%s'", expected, actual)
	}
}

func TestInterpreterDoesNotUnderstandAboutUpdatingCardFields(t *testing.T) {
	input := "{\"action\":{\"id\":\"5db1ce62f705b8839dccd1bb\",\"idMemberCreator\":\"5204f6d9688f3e662400192a\",\"data\":{\"old\":{\"name\":\"Mejorar integración trello <>mattermost\"},\"card\":{\"name\":\"Mejorar integración trello <> mattermost\",\"id\":\"5d6e9809310ed836c45d81c2\",\"idShort\":471,\"shortLink\":\"efhMxG9i\"},\"board\":{\"id\":\"55152efeca261dd348790ec6\",\"name\":\"Devscola Kanban\",\"shortLink\":\"umDj63lY\"},\"list\":{\"id\":\"57f36b227295309e6cea12a3\",\"name\":\"IN PROGRESS\"}},\"type\":\"updateCard\",\"date\":\"2019-10-24T16:16:34.839Z\",\"limits\":{},\"display\":{\"translationKey\":\"action_renamed_card\",\"entities\":{\"card\":{\"type\":\"card\",\"id\":\"5d6e9809310ed836c45d81c2\",\"shortLink\":\"efhMxG9i\",\"text\":\"Mejorar integración trello <> mattermost\"},\"name\":{\"type\":\"text\",\"text\":\"Mejorar integración trello <>mattermost\"},\"memberCreator\":{\"type\":\"member\",\"id\":\"5204f6d9688f3e662400192a\",\"username\":\"vibaiher\",\"text\":\"Vicente Baixauli\"}}},\"memberCreator\":{\"id\":\"5204f6d9688f3e662400192a\",\"activityBlocked\":false,\"avatarHash\":\"d3384d68d656f87d013cedc39b3a5382\",\"avatarUrl\":\"https://trello-avatars.s3.amazonaws.com/d3384d68d656f87d013cedc39b3a5382\",\"fullName\":\"Vicente Baixauli\",\"idMemberReferrer\":null,\"initials\":\"VB\",\"nonPublic\":{},\"nonPublicAvailable\":false,\"username\":\"vibaiher\"}}}"
	expected := "**updateCard** action not supported"

	_, err := interpreter.Interpret([]byte(input))

	actual := err.Error()
	if actual != expected {
		t.Errorf("Expected '%s', got '%s'", expected, actual)
	}
}

func TestInterpreterDoesNotUnderstandUpdatedChecklistItem(t *testing.T) {
	input := "{\"action\":{\"id\":\"5daf57f36fdb5d673838fb56\",\"idMemberCreator\":\"5204f6d9688f3e662400192a\",\"data\":{\"board\":{\"id\":\"55152efeca261dd348790ec6\",\"name\":\"DevscolaKanban\",\"shortLink\":\"umDj63lY\"},\"card\":{\"id\":\"5d6e9809310ed836c45d81c2\",\"name\":\"Mejorarintegracióntrello<>mattermost\",\"idShort\":471,\"shortLink\":\"efhMxG9i\"},\"checklist\":{\"id\":\"5d8e1c822671f91bcc194472\",\"name\":\"Checklist\"},\"checkItem\":{\"id\":\"5d8e1c923d1ada265748a2ce\",\"name\":\"Añadirtarjeta\",\"state\":\"incomplete\"}},\"type\":\"updateCheckItemStateOnCard\",\"date\":\"2019-10-22T19:26:43.115Z\",\"limits\":{},\"display\":{\"translationKey\":\"action_marked_checkitem_incomplete\",\"entities\":{\"checkitem\":{\"type\":\"checkItem\",\"nameHtml\":\"Añadir tarjeta\",\"id\":\"5d8e1c923d1ada265748a2ce\",\"state\":\"incomplete\",\"text\":\"Añadir tarjeta\"},\"card\":{\"type\":\"card\",\"id\":\"5d6e9809310ed836c45d81c2\",\"shortLink\":\"efhMxG9i\",\"text\":\"Mejorar integración trello <>mattermost\"},\"memberCreator\":{\"type\":\"member\",\"id\":\"5204f6d9688f3e662400192a\",\"username\":\"vibaiher\",\"text\":\"Vicente Baixauli\"}}},\"memberCreator\":{\"id\":\"5204f6d9688f3e662400192a\",\"activityBlocked\": false,\"avatarHash\":\"d3384d68d656f87d013cedc39b3a5382\",\"avatarUrl\":\"https://trello-avatars.s3.amazonaws.com/d3384d68d656f87d013cedc39b3a5382\",\"fullName\":\"Vicente Baixauli\",\"idMemberReferrer\": null,\"initials\":\"VB\",\"nonPublic\":{},\"nonPublicAvailable\": false,\"username\":\"vibaiher\"}}}"
	expected := "**updateCheckItemStateOnCard** action not supported"

	_, err := interpreter.Interpret([]byte(input))

	actual := err.Error()
	if actual != expected {
		t.Errorf("Expected '%s', got '%s'", expected, actual)
	}
}
