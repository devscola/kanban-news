# Kanban news

API "bridge" between Trello and Mattermost

## Requirements

- docker

## Development

- Build docker image with current code: `./scripts/build`
- Run tests: `./scripts/test`
- Start API locally: `./scripts/run`
