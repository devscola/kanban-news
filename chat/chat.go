package chat

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
)

var webhook = os.Getenv("CHAT_WEBHOOK")

// Notify sends messages to Chat
func Notify(message string) {
	if message == "" {
		return
	}

	data := map[string]string{"text": message}
	body, err := json.Marshal(data)

	resp, err := http.Post(webhook, "application/json", bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
}
