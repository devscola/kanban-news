package main

import (
	"fmt"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"gitlab.com/devscola/kanban-news/chat"
	"gitlab.com/devscola/kanban-news/interpreter"
	"gitlab.com/devscola/kanban-news/kanban"
)

func main() {
	router := gin.Default()
	router.Use(gin.Logger())

	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	router.GET("/connect", func(c *gin.Context) {
		kanban.Connect()

		c.JSON(200, gin.H{
			"message": "ok",
		})
	})

	router.HEAD("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	router.POST("/", func(c *gin.Context) {
		event, _ := ioutil.ReadAll(c.Request.Body)
		message, err := interpreter.Interpret(event)

		if err != nil {
			fmt.Println(err)
		}

		chat.Notify(message)

		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	router.Run()
}
