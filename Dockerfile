FROM golang:1.13.3-alpine AS development

ENV CGO_ENABLED 0
WORKDIR /go/src/gitlab.com/devscola/kanban-news

COPY go.mod go.sum ./
RUN go mod download

COPY . .

FROM development AS build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /opt

ENV GIN_MODE release
ENV PORT 8080

COPY --from=build /go/src/gitlab.com/devscola/kanban-news/main .

EXPOSE 8080
ENTRYPOINT ["/opt/main"]